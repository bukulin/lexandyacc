#include "symbol_table.h"

#include <stddef.h>
#include <string.h>
#include <stdlib.h>

enum {
	SYMBOL_TABLE_SIZE = 32
};

static SymbolStruct itsSymbolTable[SYMBOL_TABLE_SIZE];
static unsigned int itsNumberOfSymbols;

int SymbolTable_Create(void)
{
	memset(itsSymbolTable, 0, sizeof(itsSymbolTable));
	return 0;
}

void SymbolTable_Destroy()
{
	unsigned int i;
	for(i = 0; i < itsNumberOfSymbols; ++i)
	{
		free((void*)itsSymbolTable[i].name);
	}
	itsNumberOfSymbols = 0;
}

Symbol SymbolTable_Lookup(const char* name)
{
	unsigned int i;

	for(i = 0; i < itsNumberOfSymbols; ++i)
	{
		if (strcmp(itsSymbolTable[i].name, name) == 0)
			return &itsSymbolTable[i];
	}

	if (i == SYMBOL_TABLE_SIZE)
		return NULL;

	itsSymbolTable[itsNumberOfSymbols].name = strdup(name);
	return &itsSymbolTable[itsNumberOfSymbols++];
}
