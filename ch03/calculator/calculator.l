%{
#include "calculator.tab.h"
#include "symbol_table.h"
#include <math.h>
%}

number		([0-9]+|([0-9]*\.[0-9]+)([Ee][+-]?[0-9]+)?)
whitespace	[ \t]
symbol		[a-zA-Z][a-zA-Z0-9]*

%%

{number}	{ yylval.doubleValue = atof(yytext); return NUMBER; }
{whitespace}	;
{symbol}	{ yylval.symbol = SymbolTable_Lookup(yytext);
		  if (yylval.symbol == NULL)
			yyerror("Too many symbols");
		  return NAME;
		}
"$"		return 0;		/* End of input */

\n		|
.		return yytext[0];

%%
