#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H 1

struct SymbolStruct;

typedef double (*SymbolFunction)(double);
typedef double (*SymbolFunction2)(double, double);

struct SymbolStruct
{
	const char* name;
	SymbolFunction execute;
	SymbolFunction2 execute2;
	double value;
};
typedef struct SymbolStruct SymbolStruct;
typedef SymbolStruct* Symbol;

int SymbolTable_Create(void);
void SymbolTable_Destroy();

Symbol SymbolTable_Lookup(const char* name);

#endif
