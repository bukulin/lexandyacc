%{
#include "symbol_table.h"
#include <stdio.h>
#include <math.h>

int yyerror(char* s);

%}

%union {
	double doubleValue;
	struct SymbolStruct* symbol;
}


%token	<symbol> NAME;
%token	<doubleValue> NUMBER;

%left '+' '-'
%left '*' '/'
%nonassoc UMINUS

%type	<doubleValue>	 expression

%%

statement_list:	statement '\n'
	|		statement_list statement '\n'
	;

statement:	NAME '=' expression	{ $1->value = $3; }
	|	expression		{ printf("= %g\n", $1); }
	;

expression:	expression '+' expression	{ $$ = $1 + $3; }
	|	expression '-' expression	{ $$ = $1 - $3; }
	|	expression '*' expression	{ $$ = $1 * $3; }
	|	expression '/' expression	{
			if ($3 == 0.0f)
				yyerror("divide by zero");
			else
				$$ = $1 / $3;
		}

	|	'-'expression %prec UMINUS	{ $$ = -$2; }
	|	'(' expression ')'		{ $$ = $2; }
	|	NUMBER
	|	NAME				{ $$ = $1->value; }

	|	NAME '(' expression ')'	{
			if ($1->execute)
				$$ = $1->execute($3);
			else
			{
				printf("%s is not a function\n", $1->name);
				$$ = 0.0;
			}
		}

	|	NAME '(' expression ',' expression ')' {
			if ($1->execute2)
				$$ = $1->execute2($3, $5);
			else
			{
				printf("%s is not a function\n", $1->name);
				$$ = 0.0;
			}
		}
	;

%%

int yyerror(char* s)
{
	printf("%s\n", s);
	return 0;
}
