#include "built_in_functions.h"
#include "symbol_table.h"

#include <stddef.h>
#include <math.h>

#define ARRAY_SIZE(a) (sizeof((a))/sizeof((a[0])))

static int AddFunction(const char* name,
		       SymbolFunction function)
{
	Symbol s = SymbolTable_Lookup(name);
	if (s == NULL)
		return -1;
	s->execute = function;
	return 0;
}

static int AddFunction2(const char* name,
			SymbolFunction2 function)
{
	Symbol s = SymbolTable_Lookup(name);
	if (s == NULL)
		return -1;
	s->execute2 = function;
	return 0;
}


int BuiltInFunctions_Create()
{
	const struct {
		const char* name;
		SymbolFunction function;
	} functionMap[] = {
		{"sqrt", sqrt},
		{"log", log},
		{"exp", exp},
		{"atan", atan}
	};

	const struct {
		const char* name;
		SymbolFunction2 function2;
	} function2Map[] = {
		{"modulo", fmod},
		{"atan", atan2}
	};

	unsigned int i;
	int res = 0;

	for(i = 0; i < ARRAY_SIZE(functionMap); ++i)
	{
		res |= AddFunction(functionMap[i].name,
				   functionMap[i].function);
	}

	for(i = 0; i < ARRAY_SIZE(function2Map); ++i)
	{
		res |= AddFunction2(function2Map[i].name,
				    function2Map[i].function2);
	}

	return res;
}

void BuiltInFunctions_Destroy()
{}
