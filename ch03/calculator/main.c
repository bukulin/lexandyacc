#include "calculator.tab.h"
#include "symbol_table.h"
#include "built_in_functions.h"

int main()
{
	int res;

	res = SymbolTable_Create();
	if (res != 0)
		return res;

	res = BuiltInFunctions_Create();
	if (res != 0)
		return res;

	yyparse();

	BuiltInFunctions_Destroy();
	SymbolTable_Destroy();

	return 0;
}
