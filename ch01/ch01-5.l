%{

#include "ch01-5.tab.h"

#define LOOKUP 0

static int state;

static int add_word(const int type, const char* wordName);
static int lookup_word(const char* wordName);

%}

%%

\n		{ state = LOOKUP; }

\.\n		{ state = LOOKUP;
		  return 0; }

^verb		{ state = VERB; }
^adj		{ state = ADJECTIVE; }
^adv		{ state = ADVERB; }
^noun		{ state = NOUN; }
^prep		{ state = PREPOSITION; }
^pron		{ state = PRONOUN; }
^conj		{ state = CONJUNCTION; }

[a-zA-Z]+	{
			if (state != LOOKUP)
			{
				add_word(state, yytext);
			}
			else
			{
				switch(lookup_word(yytext))
				{
				case VERB:
					return VERB;
				case ADJECTIVE:
					return ADJECTIVE;
				case ADVERB:
					return ADVERB;
				case NOUN:
					return NOUN;
				case PREPOSITION:
					return PREPOSITION;
				case PRONOUN:
					return PRONOUN;
				case CONJUNCTION:
					return CONJUNCTION;
				default:
					printf("%s: don't recognize\n", yytext);
					break;
				}
			}
		}

.	;
%%

struct Word
{
	struct Word* next;
	const char* data;
	int type;
};

static struct Word* wordList;

static int add_word(const int type, const char* wordName)
{
	struct Word* word;

	if (lookup_word(wordName) != LOOKUP)
	{
		printf("!!! warning %s is already defined\n", wordName);
		return 0;
	}

	word = calloc(1, sizeof(*word));
	if (word == NULL)
	{
		printf("!!! error: cannot allocate memory");
		return 0;
	}

	word->data = strdup(wordName);
	if (word->data == NULL)
	{
		free(word);
		printf("!!! error: cannot duplicate string");
		return 0;
	}

	word->type = type;

	word->next = wordList;
	wordList = word;

	return 1;
}

static int lookup_word(const char* wordName)
{
	struct Word* word;

	for(word = wordList;
	    word != NULL;
	    word = word->next)
	{
		if (strcmp(wordName, word->data) == 0)
			return word->type;
	}

	return LOOKUP;
}
