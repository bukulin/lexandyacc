%{

enum {
	LOOKUP = 0,
	VERB,
	ADJ,
	ADV,
	NOUN,
	PREP,
	PRON,
	CONJ
};

static int state;
static int add_word(const int type, const char* wordName);
static int lookup_word(const char* wordName);

%}

%%

\n		{ state = LOOKUP; }

^verb		{ state = VERB; }
^adj		{ state = ADJ; }
^adv		{ state = ADV; }
^noun		{ state = NOUN; }
^prep		{ state = PREP; }
^pron		{ state = PRON; }
^conj		{ state = CONJ; }

[a-zA-Z]+	{
			if (state != LOOKUP)
			{
				add_word(state, yytext);
			}
			else
			{
				switch(lookup_word(yytext))
				{
				case VERB:
					printf("%s: verb\n", yytext);
					break;
				case ADJ:
					printf("%s: adjective\n", yytext);
					break;
				case ADV:
					printf("%s: adverb\n", yytext);
					break;
				case NOUN:
					printf("%s: noun\n", yytext);
					break;
				case PREP:
					printf("%s: preposition\n", yytext);
					break;
				case PRON:
					printf("%s: pronoun\n", yytext);
					break;
				case CONJ:
					printf("%s: conjunction\n", yytext);
					break;
				default:
					printf("%s: don't recognize\n", yytext);
					break;
				}
			}
		}

%%

struct Word
{
	struct Word* next;
	const char* data;
	int type;
};

static struct Word* wordList;

static int add_word(const int type, const char* wordName)
{
	struct Word* word;

	if (lookup_word(wordName) != LOOKUP)
	{
		printf("!!! warning %s is already defined\n", wordName);
		return 0;
	}

	word = calloc(1, sizeof(*word));
	if (word == NULL)
	{
		printf("!!! error: cannot allocate memory");
		return 0;
	}

	word->data = strdup(wordName);
	if (word->data == NULL)
	{
		free(word);
		printf("!!! error: cannot duplicate string");
		return 0;
	}

	word->type = type;

	word->next = wordList;
	wordList = word;

	return 1;
}

static int lookup_word(const char* wordName)
{
	struct Word* word;

	for(word = wordList;
	    word != NULL;
	    word = word->next)
	{
		if (strcmp(wordName, word->data) == 0)
			return word->type;
	}

	return LOOKUP;
}
