#include "parser_state.h"
#include "screens.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

extern int yyparse (void);
extern int yylex_destroy (void );


int main(const int argc, const char* argv[])
{
	int res;
	extern FILE* yyin;

	if (argc > 3)
	{
		fprintf(stderr,
			"%s: usage [infile] [outfile]\n",
			argv[0]);
		exit(1);
	}

	if (argc > 1)
	{
		const char* inFile = argv[1];
		yyin = fopen(inFile, "r");
		if (yyin == NULL)
		{
			fprintf(stderr,
				"%s: cannot open %s\n",
				argv[0], inFile);
			exit(1);
		}
	}

	res = ParserState_Init();
	if (res != 0)
		return 1;

	yyparse();
	yylex_destroy();

	if (!Screen_IsDone())
	{
		warning("Premature EOF", NULL);
		exit(1);
	}

	Screens_Run();
	Screens_TearDown();

	ParserState_Destroy();

	return 0;
}
