#include "items.h"

#include "actions.h"
#include "commands.h"

#include "mgl.tab.h"

#include <stdio.h>
#include <stdlib.h>

struct ItemStruct
{
	const char* itemString;
	Command command;
	Action action;
	int attribute;
};

Item Item_Create(const char* itemString,
		 Command command,
		 Action action,
		 const int attribute)
{
	Item self = calloc(1, sizeof(*self));
	if (self == NULL)
		return NULL;

	self->itemString = itemString;
	self->command = command;
	self->action = action;
	self->attribute = attribute;
	return self;
}

void Item_Destroy(Item self)
{
	Action_Destroy(self->action);
	Command_Destroy(self->command);
	free((char*)self->itemString);
	free(self);
}

void Item_Show(Item self, const unsigned int ordinal)
{
	if (self->attribute == INVISIBLE)
		return;

	printf("%u) %s\n",
	       ordinal,
	       self->itemString);
}

void Item_RunAction(Item self)
{
	Action_Run(self->action);
}
