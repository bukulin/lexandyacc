#include "commands.h"

#include <stdio.h>
#include <stdlib.h>

struct CommandStruct
{
	const char* id;
};

Command Command_Create(const char* id)
{
	Command self = calloc(1, sizeof(*self));
	if (self == NULL)
		return NULL;
	self->id = id;
	return self;
}

void Command_Destroy(Command self)
{
	free((char*)self->id);
	free(self);
}
