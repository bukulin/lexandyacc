#include "actions.h"
#include "screens.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct ActionInterfaceStruct
{
	void (*Destroy)(Action self);
	void (*Run)(Action self);
};
typedef struct ActionInterfaceStruct ActionInterfaceStruct;
typedef ActionInterfaceStruct* ActionInterface;

struct ActionStruct
{
	ActionInterface vtable;
};

static bool isValid(Action self)
{
	return self && self->vtable;
}

void Action_Destroy(Action self)
{
	if (isValid(self) && self->vtable->Destroy)
		self->vtable->Destroy(self);
}

void Action_Run(Action self)
{
	if (isValid(self) && self->vtable->Run)
		self->vtable->Run(self);
}



struct IgnoreActionStruct
{
	ActionStruct base;
};
typedef struct IgnoreActionStruct IgnoreActionStruct;
typedef IgnoreActionStruct* IgnoreAction;

static void IgnoreDestroy(Action base)
{
	IgnoreAction self = (IgnoreAction)base;
	free(self);
}

static void IgnoreRun(Action base __attribute__((unused)))
{}

static ActionInterfaceStruct ignoreInterface = {
	.Destroy = IgnoreDestroy,
	.Run  = IgnoreRun
};

Action IgnoreAction_Create()
{
	IgnoreAction self = calloc(1, sizeof(*self));
	if (self == NULL)
		return NULL;
	self->base.vtable = &ignoreInterface;
	return &self->base;
}






struct ExecuteActionStruct
{
	ActionStruct base;
	const char* command;
};
typedef struct ExecuteActionStruct ExecuteActionStruct;
typedef ExecuteActionStruct* ExecuteAction;

static void ExecuteDestroy(Action base)
{
	ExecuteAction self = (ExecuteAction)base;
	free((void*)self->command);
	free(self);
}

static void ExecuteRun(Action base)
{
	ExecuteAction self = (ExecuteAction)base;
	system(self->command);
}

static ActionInterfaceStruct executeInterface = {
	.Destroy = ExecuteDestroy,
	.Run  = ExecuteRun
};

Action ExecuteAction_Create(const char* str)
{
	ExecuteAction self = calloc(1, sizeof(*self));
	if (self == NULL)
		return NULL;
	self->command = str;
	self->base.vtable = &executeInterface;
	return &self->base;
}





struct MenuActionStruct
{
	ActionStruct base;
	const char* menuId;
};
typedef struct MenuActionStruct MenuActionStruct;
typedef MenuActionStruct* MenuAction;

static void MenuDestroy(Action base)
{
	MenuAction self = (MenuAction)base;
	free((char*)self->menuId);
	free(self);
}

static void MenuRun(Action base)
{
	MenuAction self = (MenuAction)base;
	Screens_Select(self->menuId);
}

static ActionInterfaceStruct menuInterface = {
	.Destroy = MenuDestroy,
	.Run = MenuRun
};

Action MenuAction_Create(const char* id)
{
	MenuAction self = calloc(1, sizeof(*self));
	if (self == NULL)
		return NULL;
	self->menuId = id;
	self->base.vtable = &menuInterface;
	return &self->base;
}





struct QuitActionStruct
{
	ActionStruct base;
};
typedef struct QuitActionStruct QuitActionStruct;
typedef QuitActionStruct* QuitAction;

static void QuitDestroy(Action base)
{
	QuitAction self = (QuitAction)base;
	free(self);
}

static void QuitRun(Action base __attribute__((unused)))
{
	Screens_Quit();
}

static ActionInterfaceStruct quitInterface = {
	.Destroy = QuitDestroy,
	.Run = QuitRun
};

Action QuitAction_Create()
{
	QuitAction self = calloc(1, sizeof(*self));
	if (self == NULL)
		return NULL;
	self->base.vtable = &quitInterface;
	return &self->base;
}
