#include "titles.h"

#include <stdio.h>
#include <stdlib.h>

struct TitleStruct
{
	const char* title;
};

Title Title_Create(const char* title)
{
	Title self = calloc(1, sizeof(*self));
	self->title = title;
	return self;
}

void Title_Destroy(Title self)
{
	free((void*)self->title);
	free(self);
}

void Title_Show(Title self)
{
	printf("---- %s ----\n", self->title);
}
