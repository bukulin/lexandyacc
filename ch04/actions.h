#ifndef ACTIONS_H
#define ACTIONS_H 1

struct ActionStruct;
typedef struct ActionStruct ActionStruct;
typedef ActionStruct* Action;

void Action_Destroy(Action self);
void Action_Run(Action self);

Action IgnoreAction_Create();
Action ExecuteAction_Create(const char* string);
Action MenuAction_Create(const char* id);
Action QuitAction_Create();

#endif
