#ifndef SCREENS_H
#define SCREENS_H 1

#include <stdbool.h>

struct TitleStruct;
struct ItemStruct;

int Screen_Start(const char* id);
int Screen_End(const char* id);

bool Screen_IsDone(void);
void Screen_AddTitle(struct TitleStruct* title);
void Screen_AddItem(struct ItemStruct* item);


void Screens_Run();
void Screens_TearDown();
void Screens_Select(const char* id);
void Screens_Quit();

#endif
