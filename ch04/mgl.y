%{
#include "actions.h"
#include "commands.h"
#include "items.h"
#include "parser_state.h"
#include "screens.h"
#include "titles.h"

#include <stdio.h>
#include <string.h>
%}

%union {
	char* string;
	int cmd;
	struct ActionStruct* action;
	struct CommandStruct* command;
}

%token	<cmd>		SCREEN TITLE ITEM COMMAND ACTION EXECUTE EMPTY
%token	<cmd>		MENU QUIT IGNORE ATTRIBUTE VISIBLE INVISIBLE END
%token	<string>	QSTRING ID

%type	<action>	action
%type	<command>	command
%type	<cmd>		line attribute
%type	<string>	id qstring

%start screens

%%

screens:	/* empty */
	|	screens screen
	;

screen:	screen_name screen_contents screen_terminator
	|	screen_name screen_terminator
	;

screen_name:
		SCREEN ID	{ Screen_Start($2); }
	|	SCREEN		{ Screen_Start(strdup("default")); }
	;

screen_terminator:
		END ID		{ Screen_End($2); }
	|	END		{ Screen_End(strdup("default")); }
	;

screen_contents:
		titles lines
	;

titles:	/* empty */
	|	titles title
	;

title:		TITLE qstring	{ Screen_AddTitle( Title_Create($2) ); }
	;

lines:		line
	|	lines line
	;

line:		ITEM qstring command ACTION action attribute	{ Screen_AddItem( Item_Create($2, $3, $5, $6) ); $$ = ITEM; }
	;

command:	/* empty */	{ $$ = Command_Create(strdup("")); }
	|	COMMAND ID	{ $$ = Command_Create($2); }
	;

action:	IGNORE			{ $$ = IgnoreAction_Create(); }
	|	EXECUTE qstring	{ $$ = ExecuteAction_Create($2); }
	|	MENU ID		{ $$ = MenuAction_Create($2); }
	|	QUIT			{ $$ = QuitAction_Create(); }
	;

attribute:	/* empty */		{ $$ = VISIBLE; }
	|	ATTRIBUTE VISIBLE	{ $$ = VISIBLE; }
	|	ATTRIBUTE INVISIBLE	{ $$ = INVISIBLE; }
	;

qstring:	QSTRING	{ $$ = $1; }
	|	ID		{ warning("Non-string literal inappropriate", NULL); $$ = $1; }
	;

id:		ID		{ $$ = $1; }
	|	QSTRING	{ warning("String literal inappropriate", NULL); $$ = $1; }
	;


%%
