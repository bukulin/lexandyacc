#include "screens.h"
#include "titles.h"
#include "items.h"
#include "parser_state.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct ScreenStruct
{
	const char* id;

	Title titles[16];
	size_t numberOfTitles;

	Item items[16];
	size_t numberOfItems;
};
typedef struct ScreenStruct ScreenStruct;
typedef ScreenStruct* Screen;

static Screen Screen_Create(const char* id)
{
	Screen self = calloc(1, sizeof(*self));
	self->id = id;
	return self;
}

static void Screen_Destroy(Screen self)
{
	size_t i;

	for(i = 0; i < self->numberOfItems; ++i)
		Item_Destroy(self->items[i]);

	for(i = 0; i < self->numberOfTitles; ++i)
		Title_Destroy(self->titles[i]);

	free((char*)self->id);
	free(self);
}

static void _Screen_AddTitle(Screen self, Title title)
{
	self->titles[self->numberOfTitles] = title;
	self->numberOfTitles += 1;
}

static void _Screen_AddItem(Screen self, Item item)
{
	self->items[self->numberOfItems] = item;
	self->numberOfItems += 1;
}

static void Screen_Show(Screen self)
{
	size_t i;

	for(i = 0; i < self->numberOfTitles; ++i)
		Title_Show(self->titles[i]);

	for(i = 0; i < self->numberOfItems; ++i)
	{
		Item_Show(self->items[i], i + 1);
	}
}

static void Screen_RunItem(Screen self, const size_t selected)
{
	if (selected >= self->numberOfItems)
		return;
	Item_RunAction(self->items[selected]);
}


struct ScreenListElemStruct
{
	struct ScreenListElemStruct* next;
	Screen data;
};
typedef struct ScreenListElemStruct ScreenListElemStruct;
typedef ScreenListElemStruct* ScreenListElem;


static int ScreenList_Push(ScreenListElem* head, Screen screen)
{
	ScreenListElem elem = calloc(1, sizeof(*elem));
	if (elem == NULL)
		return -1;
	elem->data = screen;
	elem->next = *head;
	*head = elem;
	return 0;
}

static Screen ScreenList_Pop(ScreenListElem* head)
{
	ScreenListElem elem = *head;
	if (elem == NULL)
		return NULL;

	Screen screen = elem->data;
	*head = elem->next;
	free(elem);
	return screen;
}


static ScreenListElem itsScreens;
static Screen itsSelectedScreen;
static Screen itsWorkScreen;
static bool itsKeep;

int Screen_Start(const char* id)
{
	itsWorkScreen = Screen_Create(id);
	return 0;
}

int Screen_End(const char* id)
{
	if (strcmp(id, itsWorkScreen->id) != 0)
	{
		warning("Unmatching END of screen", NULL);
	}
	free((char*)id);

	if (itsSelectedScreen == NULL)
		itsSelectedScreen = itsWorkScreen;

	ScreenList_Push(&itsScreens, itsWorkScreen);
	itsWorkScreen = NULL;
	return 0;
}

bool Screen_IsDone(void)
{
	return itsWorkScreen == NULL;
}

void Screen_AddTitle(struct TitleStruct* title)
{
	_Screen_AddTitle(itsWorkScreen, title);
}

void Screen_AddItem(struct ItemStruct* item)
{
	_Screen_AddItem(itsWorkScreen, item);
}

static size_t GetSelection(void)
{
	size_t selected;
	scanf("%zu", &selected);
	return selected;
}

void Screens_Run()
{
	itsKeep = true;

	while(itsKeep)
	{
		int selected;
		Screen_Show(itsSelectedScreen);
		selected = GetSelection();
		Screen_RunItem(itsSelectedScreen, selected - 1);
	}
}

void Screens_TearDown()
{
	Screen screen;

	for (screen = ScreenList_Pop(&itsScreens);
	     screen != 0;
	     screen = ScreenList_Pop(&itsScreens))
	{
		Screen_Destroy(screen);
	}
}

void Screens_Select(const char* id)
{
	ScreenListElem elem;

	for(elem = itsScreens;
	    elem != NULL;
	    elem = elem->next)
	{
		if (strcmp(id, elem->data->id) != 0)
			continue;
		itsSelectedScreen = elem->data;
		return;
	}
}

void Screens_Quit(void)
{
	itsKeep = false;
}
