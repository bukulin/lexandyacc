#ifndef ITEMS_H
#define ITEMS_H 1

struct ActionStruct;
struct CommandStruct;


struct ItemStruct;
typedef struct ItemStruct ItemStruct;
typedef ItemStruct* Item;

Item Item_Create(const char* itemString,
		 struct CommandStruct* command,
		 struct ActionStruct* action,
		 const int attribute);
void Item_Destroy(Item self);

void Item_Show(Item self, const unsigned int ordinal);
void Item_RunAction(Item self);

#endif
