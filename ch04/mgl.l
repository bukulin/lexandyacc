%{
#include "mgl.tab.h"
#include "parser_state.h"
%}

ws [ \t]+
comment #.*
qstring \"[^\"\n]*[\"\n]
id [a-zA-Z][a-zA-Z0-9]*
nl \n

%%

{ws}		;
{comment}	;

{qstring}	{ yylval.string = strdup(yytext + 1);
		  if(yylval.string[yyleng - 2] != '"')
			warning("Unterminated character string", NULL);
		  else
			yylval.string[yyleng - 2] = '\0';
		  return QSTRING; }

action		{ return ACTION; }
attribute	{ return ATTRIBUTE; }
command	{ return COMMAND; }
end		{ return END; }
execute	{ return EXECUTE; }
ignore		{ return IGNORE; }
invisible	{ return INVISIBLE; }
item		{ return ITEM; }
menu		{ return MENU; }
quit		{ return QUIT; }
screen		{ return SCREEN; }
title		{ return TITLE; }
visible	{ return VISIBLE; }


{id}		{ yylval.string = strdup(yytext);
		  return ID; }

{nl}		{ ParserState_NextLine(); }
.		{ return yytext[0]; }

%%
