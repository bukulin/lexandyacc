#include "parser_state.h"
#include <stdio.h>

static unsigned int lineno;

int ParserState_Init(void)
{
	lineno = 0;
	return 0;
}

void ParserState_Destroy()
{
	extern FILE* yyin;
	extern FILE* yyout;

	if (yyin && yyin != stdin)
	{
		fclose(yyin);
		yyin = NULL;
	}

	if (yyout && yyout != stdout)
	{
		fclose(yyout);
		yyout = NULL;
	}
}

void ParserState_NextLine()
{
	++lineno;
}

void warning(const char* message, const char* ptr)
{
	fprintf(stderr, "%s", message);
	if (ptr)
		fprintf(stderr, " %s", ptr);
	fprintf(stderr, " line %u\n", lineno);
}

void yyerror(char* s)
{
	fprintf(stderr, "%s\n", s);
}
