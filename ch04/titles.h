#ifndef TITLES_H
#define TITLES_H 1

struct TitleStruct;
typedef struct TitleStruct TitleStruct;
typedef TitleStruct* Title;

Title Title_Create(const char* title);
void Title_Destroy(Title self);

void Title_Show(Title self);

#endif
