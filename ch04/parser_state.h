#ifndef PARSER_STATE_H
#define PARSER_STATE_H 1

int ParserState_Init();
void ParserState_Destroy();

void ParserState_NextLine();

void warning(const char* message, const char* ptr);

#endif
