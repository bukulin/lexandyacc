#ifndef COMMANDS_H
#define COMMANDS_H 1

struct CommandStruct;
typedef struct CommandStruct CommandStruct;
typedef CommandStruct* Command;

Command Command_Create(const char* id);
void Command_Destroy(Command self);

#endif
