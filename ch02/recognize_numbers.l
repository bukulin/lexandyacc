%%

[\n\t ]	;
[-+]?(([0-9]+)|([0-9]*\.[0-9]+)([eE][+-]?[0-9]+)?)	{ printf("number\n"); }
#.*		{ printf("comment\n"); }
\"[^"\n]*["\n]	{ printf("quoted text: '%s'", yytext); }
.		ECHO;

%%

main()
{
	yylex();
}
