%{
#include <stdio.h>

static unsigned int charCount;
static unsigned int wordCount;
static unsigned int lineCount;

#undef yywrap
%}

word [^ \t \n]+
eol \n

%%

{word}		{ wordCount++; charCount += yyleng; }
{eol}		{ charCount++; lineCount++; }
.		charCount++;

%%

static const char** fileList;
static unsigned int nFiles;
static unsigned currentFile;
static unsigned totalCharCount;
static unsigned totalWordCount;
static unsigned totalLineCount;

int main(const int argc, const char* argv[])
{
	FILE* file;

	fileList = argv + 1;
	nFiles = argc - 1;

	if (argc == 2)
	{
		/* Do not print summary if only one file is given */
		currentFile = 1;
		file = fopen(argv[1], "r");
		if (!file)
		{
			fprintf(stderr, "could not open %s\n", argv[1]);
			exit(1);
		}

		yyin = file;
	}

	if (argc > 2)
		yywrap(); /* Open first file */

	yylex();

	if (argc > 2)
	{
		printf("%8u %8u %8u %s\n",
		       lineCount, wordCount, charCount,
		       fileList[currentFile - 1]);
		totalCharCount += charCount;
		totalWordCount += wordCount;
		totalLineCount += lineCount;
		printf("%8u %8u %8u\n",
		       totalLineCount, totalWordCount, totalCharCount);
	}
	else
	{
		printf("%8u %8u %8u\n",
		       lineCount, wordCount, charCount);
	}

	return 0;
}

int yywrap(void)
{
	FILE* file = NULL;

	if (currentFile != 0 &&
	    nFiles > 1 &&
	    currentFile < nFiles)
	{
		printf("%8u %8u %8u %s\n",
		       lineCount, wordCount, charCount,
		       fileList[currentFile - 1]);
		totalCharCount += charCount;
		totalWordCount += wordCount;
		totalLineCount += lineCount;
		charCount = wordCount = lineCount = 0;
		fclose(yyin);
	}

	while (fileList[currentFile] != (char*)0)
	{
		file = fopen(fileList[currentFile++], "r");
		if (file != NULL)
		{
			yyin = file;
			break;
		}
		fprintf(stderr, "could not open %s\n", fileList[currentFile - 1]);
	}
	return file ? 0 : 1;
}
