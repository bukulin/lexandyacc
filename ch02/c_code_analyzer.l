%{
static unsigned int comments;
static unsigned int code;
static unsigned int whitespace;
%}

%x COMMENT

%%

^[\t ]*"/*"			{ BEGIN COMMENT; }	/* Start of a comment */
^[\t ]*"/*".*"*/"[\t ]*\n	{ comments++; }	/* Self-contained comment */
<COMMENT>"*/"[\t ]*\n		{ BEGIN 0; comments++; }	/* End of comment */
<COMMENT>"*/"			{ BEGIN 0; }		/* Continue processing after comment */
<COMMENT>\n			{ comments++; }	/* Avoiding buffer overflow in comment state */
<COMMENT>.\n			{ comments++; }	/* Avoiding buffer overflow in comment state */

^[\t ]*\n			{ whitespace++; }	/* whitespace lines */

.+"/*".*"*/".*\n		{ code++; }		/* Something then a comment */
.*"/*".*"*/".+\n		{ code++; }		/* Comment then something */
.+"/*".*\n			{ code++; BEGIN COMMENT; }	/* Something then a comment */
.\n				{ code++; }

.				/* anything else */

%%

int main(void)
{
	yylex();
	printf("code: %u, comments: %u, whitespace: %u\n",
	       code, comments, whitespace);
	return 0;
}
